﻿
using System;
using System.Text;

class MazeGame
{
    public static MazeGame instance;
    /*
     路線:0
        牆壁:1          
        起始:2
        終點:3
        玩家:4
        走過:5
    */
    public int[,] maze;
    public int xCount;
    public int yCount;
    Random rd;

    public MazeGame(int x, int y)
    {
        instance = this;
        rd = new Random();
        CreateMaze(x, y);
        ShowMaze();
    }
    //創建地圖
    public void CreateMaze(int xCount, int yCount)
    {
        this.xCount = xCount;
        this.yCount = yCount;
        maze = new int[xCount, yCount];
        for (int x = 0; x < xCount; x++)
            for (int y = 0; y < yCount; y++)
                maze[x, y] = 1;

        //起點+終點
        maze[0, 0] = 2;
        maze[xCount - 1, yCount - 1] = 3;

        int xFlag = 0;
        int yFlag = 0;

        //右上 1 2
        bool loopFlag = true;
        while (loopFlag)//一條沒有分支的隨機路線
        {
            int rdValue = rd.Next(1, 4);
            switch (rdValue)
            {
                case 1:
                    if (xFlag + 1 < xCount)
                    {
                        if (maze[xFlag + 1, yFlag] == 1)
                        {
                            xFlag = xFlag + 1;
                            maze[xFlag, yFlag] = 0;
                        }
                        else if (maze[xFlag + 1, yFlag] == 3)
                            loopFlag = false;
                    }
                    break;
                case 2:
                    if (yFlag + 1 < yCount)
                    {
                        if (maze[xFlag, yFlag + 1] == 1)
                        {
                            yFlag = yFlag + 1;
                            maze[xFlag, yFlag] = 0;
                        }
                        else if (maze[xFlag, yFlag + 1] == 3)
                            loopFlag = false;
                    }
                    break;
            }
        }

        AddBranchs();//增加分支
    }
    //增加錯誤的路
    void AddBranchs()
    {
        for (int x = 0; x < xCount; x++)
        {
            for (int y = 0; y < yCount; y++)
            {
                if (maze[x, y] == 0)
                {
                    if (rd.Next(0, 2) == 0)//多少機率會長出分枝
                    {
                        int branchCount = rd.Next(1, 100);//分支可能的長度
                        CreateBranch(x, y, branchCount);
                    }
                }
            }
        }

    }
    //增加的分支
    void CreateBranch(int x, int y, int bCount)
    {
        int xFlag = x;
        int yFlag = y;
        for (int i = 0; i < bCount; i++)
        {
            //左下右上 1 2 3 4
            int rdValue = rd.Next(1, 5);
            switch (rdValue)
            {
                case 1:
                    if (xFlag - 1 > 0)//不是牆壁的判斷
                    {
                        if (maze[xFlag - 1, yFlag] == 1)
                        {
                            if (ObjAround(xFlag - 1, yFlag))
                            {
                                xFlag = xFlag - 1;
                                maze[xFlag, yFlag] = 0;
                            }
                        }
                    }
                    break;
                case 2:
                    if (yFlag - 1 > 0)
                    {
                        if (maze[xFlag, yFlag - 1] == 1)
                        {
                            if (ObjAround(xFlag, yFlag - 1))
                            {
                                yFlag = yFlag - 1;
                                maze[xFlag, yFlag] = 0;
                            }
                        }

                    }
                    break;
                case 3:
                    if (xFlag + 1 < xCount)
                    {
                        if (maze[xFlag + 1, yFlag] == 1)
                        {
                            if (ObjAround(xFlag + 1, yFlag))
                            {
                                xFlag = xFlag + 1;
                                maze[xFlag, yFlag] = 0;
                            }
                        }
                    }

                    break;
                case 4:
                    if (yFlag + 1 < yCount)
                    {
                        if (maze[xFlag, yFlag + 1] == 1)
                        {
                            if (ObjAround(xFlag, yFlag + 1))
                            {
                                yFlag = yFlag + 1;
                                maze[xFlag, yFlag] = 0;
                            }

                        }
                    }
                    break;
            }
        }
    }
    //物件周圍有沒有多餘一條的路線
    bool ObjAround(int x, int y)
    {
        int flag = 0;
        //左下右上 1 2 3 4

        if (x + 1 < xCount)
        {
            if (maze[x + 1, y] == 0)
                flag++;
        }

        if (x - 1 >= 0)
        {
            if (maze[x - 1, y] == 0)
                flag++;
        }

        if (y + 1 < yCount)
        {
            if (maze[x, y + 1] == 0)
                flag++;
        }

        if (y - 1 >= 0)
        {
            if (maze[x, y - 1] == 0)
                flag++;
        }

        if (flag > 1)
            return false;

        return true;
    }
    public void ShowMaze()
    {
        StringBuilder strs = new StringBuilder();

        for (int y = yCount - 1; y >= 0; y--)
        {
            //strs.Append("(" + (y) + ")  ");

            for (int x = 0; x < xCount; x++)
            {
                strs.Append(maze[x, y] + " ");
            }
            strs.Append("\n");
        }
        Console.WriteLine(strs);
    }


}


