﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class MazeManager : MonoBehaviour {

    public Transform canvasParent;

    //入口、出口、道路、牆壁

    int xFlag = 100;
    int yFlag = 100;
    GameObject[,] gos;

    // Use this for initialization
    void Start () {
        new MazeGame(xFlag, yFlag);
        gos = new GameObject[xFlag, yFlag];
        int[,] maze = MazeGame.instance.maze;

        for (int x = 0; x < xFlag; x++)
        {
            for (int y = 0; y < yFlag; y++)
            {
                GameObject go = (GameObject)Instantiate(Resources.Load(maze[x, y] + ""), new Vector2(x * 10, y * 10), Quaternion.identity);
                go.GetComponent<Transform>().SetParent(canvasParent);
                gos[x, y] = go;
            }
        }


        StartCoroutine(GoMaze());//開始跑地圖

	}

 	
	// Update is called once per frame
	void Update () {
	
	}


    int playerX = 0;
    int playerY = 0;
    Stack<RoadData> stack;
    IEnumerator  GoMaze()
    {
        yield return new WaitForSeconds(1f);
     
        stack = new Stack<RoadData>();

        while (true)
        {
            int flag = 0;

            //右左上下

            if (playerX + 1 < MazeGame.instance.xCount)
            {
                if (MazeGame.instance.maze[playerX + 1, playerY] == 0)
                {
                    stack.Push(new RoadData(playerX + 1, playerY));//將路線存入
                    flag++;
                }
                else if (MazeGame.instance.maze[playerX + 1, playerY] == 3)//到達終點
                {
                    break;
                }

            }

            if (playerX - 1 >= 0)
            {
                if (MazeGame.instance.maze[playerX - 1, playerY] == 0)
                {
                    stack.Push(new RoadData(playerX - 1, playerY));//將路線存入
                    flag++;
                }
                else if (MazeGame.instance.maze[playerX - 1, playerY] == 3)//到達終點
                {
                    break;
                }

            }

            if (playerY + 1 < MazeGame.instance.yCount)
            {
                if (MazeGame.instance.maze[playerX, playerY + 1] == 0)
                {
                    stack.Push(new RoadData(playerX, playerY + 1));//將路線存入
                    flag++;
                }
                else if (MazeGame.instance.maze[playerX, playerY + 1] == 3)//到達終點
                {
                    break;
                }

            }

            if (playerY - 1 >= 0)
            {
                if (MazeGame.instance.maze[playerX, playerY - 1] == 0)
                {
                    stack.Push(new RoadData(playerX, playerY - 1));//將路線存入
                    flag++;
                }
                else if (MazeGame.instance.maze[playerX, playerY - 1] == 3)//到達終點
                {
                    break;
                }
            }

            if (flag > 0)//代表有路走
            {
                RoadData roadData = stack.Pop();

                MazeGame.instance.maze[roadData.x, roadData.y] = 5;
                playerX = roadData.x;
                playerY = roadData.y;
                gos[playerX, playerY].GetComponent<Image>().color = new Color32(255, 0, 0, 255);
            }
            else//沒路走要走回頭路
            {
                RoadData roadData = stack.Pop();

                MazeGame.instance.maze[roadData.x, roadData.y] = 5;
                playerX = roadData.x;
                playerY = roadData.y;
                gos[playerX, playerY].GetComponent<Image>().color = new Color32(170, 0, 0, 255);

            }

            yield return new WaitForSeconds(0.01f);
        }

    }

}

class RoadData
{
    public int x;
    public int y;
    public RoadData(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
}
class Player
{
    int xFlag = 0;
    int yFlag = 0;
    Stack<RoadData> stack;
    public void GoMaze()
    {
        stack = new Stack<RoadData>();

        while (true)
        {
            int flag = 0;

            //左下右上 1 2 3 4

            if (xFlag + 1 < MazeGame.instance.xCount)
            {
                if (MazeGame.instance.maze[xFlag + 1, yFlag] == 0)
                {
                    stack.Push(new RoadData(xFlag + 1, yFlag));//將路線存入
                    flag++;
                }
                else if (MazeGame.instance.maze[xFlag + 1, yFlag] == 3)//到達終點
                {
                    break;
                }

            }

            if (xFlag - 1 >= 0)
            {
                if (MazeGame.instance.maze[xFlag - 1, yFlag] == 0)
                {
                    stack.Push(new RoadData(xFlag - 1, yFlag));//將路線存入
                    flag++;
                }
                else if (MazeGame.instance.maze[xFlag - 1, yFlag] == 3)//到達終點
                {
                    break;
                }

            }

            if (yFlag + 1 < MazeGame.instance.yCount)
            {
                if (MazeGame.instance.maze[xFlag, yFlag + 1] == 0)
                {
                    stack.Push(new RoadData(xFlag, yFlag + 1));//將路線存入
                    flag++;
                }
                else if (MazeGame.instance.maze[xFlag, yFlag + 1] == 3)//到達終點
                {
                    break;
                }

            }

            if (yFlag - 1 >= 0)
            {
                if (MazeGame.instance.maze[xFlag, yFlag - 1] == 0)
                {
                    stack.Push(new RoadData(xFlag, yFlag - 1));//將路線存入
                    flag++;
                }
                else if (MazeGame.instance.maze[xFlag, yFlag - 1] == 3)//到達終點
                {
                    break;
                }
            }

            if (flag > 0)//代表有路走
            {
                RoadData roadData = stack.Pop();

                MazeGame.instance.maze[roadData.x, roadData.y] = 5;
                xFlag = roadData.x;
                yFlag = roadData.y;
            }
            else//沒路走要走回頭路
            {
                RoadData roadData = stack.Pop();

                MazeGame.instance.maze[roadData.x, roadData.y] = 5;
                xFlag = roadData.x;
                yFlag = roadData.y;
            }
        }

    }
}


