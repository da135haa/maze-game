﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

//DFS版本
public class MazeManager_DFS : MonoBehaviour {

    public Transform canvasParent;

    //入口、出口、道路、牆壁

    int xFlag = 50;
    int yFlag = 50;
    GameObject[,] gos;

    // Use this for initialization
    void Start () {
        new MazeGame_DFS(xFlag, yFlag);
        
        gos = new GameObject[xFlag * 2 + 1, yFlag * 2 + 1];
        Node[,] maze = MazeGame_DFS.instance.mazeNodes;

        for (int x = 0; x < xFlag * 2 + 1; x++)
        {
            for (int y = 0; y < yFlag * 2 + 1; y++)
            {
                GameObject go = (GameObject)Instantiate(Resources.Load(maze[x, y].flagType + ""), new Vector2(x * 10, y * 10), Quaternion.identity);
                go.GetComponent<Transform>().SetParent(canvasParent);
                gos[x, y] = go;
            }
        }
        StartCoroutine(GoMaze());//開始跑地圖
	}
 	
	// Update is called once per frame
	void Update () {
	
	}

    int playerX = 1;
    int playerY = 1;
    Stack<RoadData> stack;
    
    IEnumerator  GoMaze()
    {
        yield return new WaitForSeconds(1f);
     
        stack = new Stack<RoadData>();

        while (true)
        {
            int flag = 0;

            //右左上下

            if (playerX + 1 < MazeGame_DFS.instance.xCount)
            {
                if (MazeGame_DFS.instance.mazeNodes[playerX + 1, playerY].flagType == 0)
                {
                    stack.Push(new RoadData(playerX + 1, playerY));//將路線存入
                    flag++;
                }
                else if (MazeGame_DFS.instance.mazeNodes[playerX + 1, playerY].flagType == 3)//到達終點
                {
                    break;
                }

            }

            if (playerX - 1 >= 0)
            {
                if (MazeGame_DFS.instance.mazeNodes[playerX - 1, playerY].flagType == 0)
                {
                    stack.Push(new RoadData(playerX - 1, playerY));//將路線存入
                    flag++;
                }
                else if (MazeGame_DFS.instance.mazeNodes[playerX - 1, playerY].flagType == 3)//到達終點
                {
                    break;
                }

            }

            if (playerY + 1 < MazeGame_DFS.instance.yCount)
            {
                if (MazeGame_DFS.instance.mazeNodes[playerX, playerY + 1].flagType == 0)
                {
                    stack.Push(new RoadData(playerX, playerY + 1));//將路線存入
                    flag++;
                }
                else if (MazeGame_DFS.instance.mazeNodes[playerX, playerY + 1].flagType == 3)//到達終點
                {
                    break;
                }

            }

            if (playerY - 1 >= 0)
            {
                if (MazeGame_DFS.instance.mazeNodes[playerX, playerY - 1].flagType == 0)
                {
                    stack.Push(new RoadData(playerX, playerY - 1));//將路線存入
                    flag++;
                }
                else if (MazeGame_DFS.instance.mazeNodes[playerX, playerY - 1].flagType == 3)//到達終點
                {
                    break;
                }
            }

            if (flag > 0)//代表有路走
            {
                RoadData roadData = stack.Pop();

                MazeGame_DFS.instance.mazeNodes[roadData.x, roadData.y].flagType = 5;
                playerX = roadData.x;
                playerY = roadData.y;
                gos[playerX, playerY].GetComponent<Image>().color = new Color32(255, 0, 0, 255);
            }
            else//沒路走要走回頭路
            {
                RoadData roadData = stack.Pop();

                MazeGame_DFS.instance.mazeNodes[roadData.x, roadData.y].flagType = 5;
                playerX = roadData.x;
                playerY = roadData.y;
                gos[playerX, playerY].GetComponent<Image>().color = new Color32(170, 0, 0, 255);

            }

            yield return new WaitForSeconds(0.01f);
        }

    }

}


