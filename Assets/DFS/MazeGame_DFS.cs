﻿using Boo.Lang;

class MazeGame_DFS
{
    public static MazeGame_DFS instance;
    /*
     路線:0
        牆壁:1          
        起始:2
        終點:3
        玩家:4
        走過:5
    */
    public Node[,] mazeNodes;
    public int xCount;
    public int yCount;
    System.Random rd;

    public MazeGame_DFS(int x, int y)
    {
        instance = this;
        rd = new System.Random();
        CreateMaze(x, y);
    }

    //創建地圖(大小會是設定值的2倍)
    public void CreateMaze(int x_value, int y_value)
    {
        //先創建空的棋盤格
        xCount = x_value * 2 + 1;
        yCount = y_value * 2 + 1;
        mazeNodes = new Node[xCount, yCount];

        //建立全部節點
        for (int x = 0; x < xCount; x++)
        {
            for (int y = 0; y < yCount; y++)
            {
                if(x == 0 || y == 0 || y == yCount - 1 || (y + 1) % 2 > 0 || (x + 1) % 2 > 0)
                {
                    mazeNodes[x, y] = new Node(1,x,y);
                }
                else
                {
                    mazeNodes[x, y] = new Node(0, x, y);
                }                 
            }               
        }
            
        //起點+終點
        mazeNodes[1, 1].flagType = 2;
        mazeNodes[xCount - 2, yCount - 2].flagType = 3;

        Generate(mazeNodes[1, 1], null);
    }

    //找到下一條路並且進去,要是沒找到就退回
    void Generate(Node n,Node from)
    {
        int pointFlag = 0;
        List<int[]> bufList = new List<int[]>();

        //判斷有哪些路可以走(遇到牆壁的判斷)
        //右、左、上、下
        int[,] nodeList = new int[,] { { n.getX() + 2, n.getY() }, { n.getX() - 2, n.getY() }, { n.getX(), n.getY() + 2 }, { n.getX(), n.getY() - 2 } };
        for(int i = 0;i < 4;i++)
        {
            //碰到邊界時,該點標記並跳過
            if(nodeList[i, 0] < 1 || nodeList[i, 0] > xCount - 2 || nodeList[i, 1] < 1 || nodeList[i, 1] > yCount - 2)
            {
                nodeList[i, 0] = 0;
                nodeList[i, 1] = 0;
            }

            //要是自己來的那條路  (把方向標記一下)
            if (from != null && nodeList[i, 0] == from.getX() && nodeList[i, 1] == from.getY())
            {
                nodeList[i, 0] = 0;
                nodeList[i, 1] = 0;
                pointFlag = i;
            }

            //將所有可以的路記下來
            if (nodeList[i, 0] != 0 && nodeList[i, 1] != 0)
            {
                //Debug.Log(nodeList[i, 0] +","+ nodeList[i, 1]);
                bufList.Add(new int[] { nodeList[i, 0], nodeList[i, 1] });
            }
        }

        //判斷開了幾道牆
        int flag = 0;
        //右左下上的判斷
        //將所有可以的路記下來
        if (nodeList[0, 0] != 0 && nodeList[0, 1] != 0)
        {
            if(mazeNodes[n.getX() + 1, n.getY()].flagType == 0)
            {
                flag++;
            }
        }
        if (nodeList[1, 0] != 0 && nodeList[1, 1] != 0)
        {
            if (mazeNodes[n.getX() - 1, n.getY()].flagType == 0)
            {
                flag++;
            }
        }

        if (nodeList[2, 0] != 0 && nodeList[2, 1] != 0)
        {
            if (mazeNodes[n.getX(), n.getY()+1].flagType == 0)
            {
                flag++;
            }
        }

        if (nodeList[3, 0] != 0 && nodeList[3, 1] != 0)
        {
            if (mazeNodes[n.getX(), n.getY()-1].flagType == 0)
            {
                flag++;
            }
        }

        if (flag > 0)
            return;

        //到這邊了代表這條路是可以標記的,把路打通
        if (from != null)
        {
            if (pointFlag == 0)
            {
                mazeNodes[n.getX()+1, n.getY()].flagType = 0;
            }
            if (pointFlag == 1)
            {
                mazeNodes[n.getX()-1, n.getY()].flagType = 0;
            }
            if (pointFlag == 2)
            {
                mazeNodes[n.getX(), n.getY()+1].flagType = 0;
            }
            if (pointFlag == 3)
            {
                mazeNodes[n.getX(), n.getY()-1].flagType = 0;
            }
        }

        //往下一個走
        int countFlag = bufList.Count;
        for (int i = 0;i< bufList.Count; i++)
        {
            int bufRd = rd.Next(0, countFlag);//亂數走一條路
            Generate(mazeNodes[bufList[bufRd][0], bufList[bufRd][1]], n);
            countFlag--;
        }

        //開始嘗試走
        //走過就標記起來
        //要是退回原點就代表結束了
        if (from == null)
        {
            return;
        }
    }
}


