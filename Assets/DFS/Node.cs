﻿using Boo.Lang;

class Node
{
    //自己代表什麼
    public int flagType = 0;
    //自己位置
    int x = 0;
    int y = 0;

    public Node(int t, int x, int y)
    {
        flagType = t;
        this.x = x;
        this.y = y;
    }

    public int getX()
    {
        return x;
    }
    public int getY()
    {
        return y;
    }
}
